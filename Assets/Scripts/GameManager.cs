﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public GameObject trail;

    public GUISkin gameGuiSkin;

    public float guiHeightPercent;
    public float scoreWidthPercent;
    public float nameGestureWidthPercent;
    public float gestureImageHeightPercent;
    public float startTimeValue = 10;

    float guiHeight, posYgui;
    float scoreWidth, nameGestureWidth, timeWidth;
    float posXNameGesture, posXtime;
    float gestureImageHeight, posXGestureImage, posYGestureImage;

    StartGameMenu startGameMenu;
    GameOverMenu gameOverMenu;
    GestureResources gestureResources;
    GestureManager gestureManager;

    bool gameStarted = false;

    int score = 0;
    string currentGestureName = "";
    float currentStartTimeValue;
    float currentTime = 9;

    Vector2 mousePosition = Vector2.zero;


    void Start ()
    {
        startGameMenu = transform.GetComponent<StartGameMenu>();
        gameOverMenu = transform.GetComponent<GameOverMenu>();
        gestureResources = transform.GetComponent<GestureResources>();
        gestureManager = transform.GetComponent<GestureManager>();
    }


    void Update ()
    {
        if(gameStarted)
        {
            GameHandle();
        }
        else
        {
            Cursor.visible = true;
            trail.SetActive(false);
        }
	}


    void OnGUI()
    {
        if (gameStarted)
        {
            CalculateSizes();
            DrawGameGUI();
            DrawInputGesture();
        }
    }


    private void DrawInputGesture()
    {
        gestureManager.SetDrawArea(GetDrawArea());
        gestureManager.DrawInputGesture();
    }


    private Rect GetDrawArea()
    {
        return new Rect(0, guiHeight, Screen.width, Screen.height - guiHeight);
    }


    private void CalculateSizes()
    {
        guiHeight = GetHeightFromPercent(guiHeightPercent);
        posYgui = Screen.height - guiHeight;

        scoreWidth = GetWidthFromPercent(scoreWidthPercent);
        nameGestureWidth = GetWidthFromPercent(nameGestureWidthPercent);
        timeWidth = Screen.width - scoreWidth - nameGestureWidth;

        posXNameGesture = scoreWidth;
        posXtime = scoreWidth + nameGestureWidth;

        gestureImageHeight = GetHeightFromPercent(gestureImageHeightPercent);
        posXGestureImage = (Screen.width - gestureImageHeight) / 2;
        posYGestureImage = (Screen.height - guiHeight - gestureImageHeight) / 2;
    }


    private void DrawGameGUI()
    {
        GUI.skin = gameGuiSkin;
        GUI.BeginGroup(new Rect(0, posYgui, Screen.width, guiHeight));
        GUI.Box(new Rect(0, 0, scoreWidth, guiHeight), "");
        GUI.Box(new Rect(posXNameGesture, 0, nameGestureWidth, guiHeight), "");
        GUI.Box(new Rect(posXtime, 0, timeWidth, guiHeight), "");

        GUI.Label(new Rect(0, 0, scoreWidth, guiHeight), "Score: " + score.ToString());
        GUI.Label(new Rect(posXNameGesture, 0, nameGestureWidth, guiHeight), currentGestureName);
        GUI.Label(new Rect(posXtime, 0, timeWidth, guiHeight), "Time left: " + Math.Round((currentTime), 2).ToString("0.00") + " c");

        GUI.EndGroup();

        GUI.Label(new Rect(posXGestureImage, posYGestureImage, gestureImageHeight, gestureImageHeight), gestureResources.GetTexture(currentGestureName));
    }


    private void GameHandle()
    {
        currentTime -= Time.deltaTime;

        if (Input.GetMouseButtonUp(0))
        {
            gestureManager.RecognizeGesture();
            if(gestureManager.GetMatchState())
            {
                SetNextGesture();
            }
            else
            {
                gestureManager.ClearPoints();
            }
        }

        if(currentTime <= 0.0f)
        {
            GameOver();
        }

        mousePosition = Input.mousePosition;
        Rect drawArea = GetDrawArea();
        if(drawArea.Contains(mousePosition) && Input.GetMouseButton(0))
        {
            Cursor.visible = false;
            trail.SetActive(true);
        }
        else
        {
            Cursor.visible = true;
            trail.SetActive(false);
        }
    }


    private void GameOver()
    {
        gameStarted = false;

        string headerMenu = "Game Over\nYour Score: " + score.ToString();
        gameOverMenu.SetHeader(headerMenu);

        gameOverMenu.enabled = true;
    }


    private void SetNextGesture()
    {
        gestureManager.ClearPoints();

        currentGestureName = gestureResources.GetRandomGesture();
        gestureManager.SetCurrentGestureName(currentGestureName);

        currentStartTimeValue = currentStartTimeValue - (currentStartTimeValue * 0.1f);
        currentTime = currentStartTimeValue;

        score++;
    }


    public void StartGame()
    {
        score = 0;
        startGameMenu.enabled = false;
        gameOverMenu.enabled = false;
        gameStarted = true;
        currentGestureName = gestureResources.GetRandomGesture();
        gestureManager.SetCurrentGestureName(currentGestureName);
        currentStartTimeValue = startTimeValue;
        currentTime = currentStartTimeValue;
    }


    float GetWidthFromPercent(float percent)
    {
        return (Screen.width * percent) / 100.0f;
    }


    float GetHeightFromPercent(float percent)
    {
        return (Screen.height * percent) / 100.0f;
    }


    public int GetScore()
    {
        return score;
    }
}
