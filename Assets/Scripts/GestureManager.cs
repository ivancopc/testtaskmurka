﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using PDollarGestureRecognizer;

public class GestureManager : MonoBehaviour
{
    public Texture2D lineTexture;

    RuntimePlatform platform;
    Vector2 virtualKeyPosition = Vector2.zero;

    private List<Point> points = new List<Point>();
    List<Gesture> trainingSet = new List<Gesture>();

    int strokeId = -1;
    int currentStrokeIdCount = 0;

    Rect drawArea = new Rect(0, 0, Screen.width, Screen.height);

    string pathToXML;
    string fileName = "";
    string currentGestureName = "";

    bool gestureIsMatch = false;


    void Start ()
    {
        platform = Application.platform;

        pathToXML = Application.dataPath + "/GesturesData";


        string[] filePaths = Directory.GetFiles(pathToXML, "*.xml");
        foreach (string filePath in filePaths)
            trainingSet.Add(GestureIO.ReadGestureFromFile(filePath));
    }


    void Update()
    {
        if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer)
        {
            if (Input.touchCount > 0)
            {
                virtualKeyPosition = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
            }
        }
        else {
            if (Input.GetMouseButton(0))
            {
                virtualKeyPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
        }


        if(Input.GetMouseButtonUp(0) && points.Count > 0)
        {
            RecognizeGesture();
        }

        if (drawArea.Contains(virtualKeyPosition))
        {

            if (Input.GetMouseButtonDown(0))
            {
                ++strokeId;
                currentStrokeIdCount = 0;
            }

            if (Input.GetMouseButton(0))
            {
                Point lastPoint = new Point(virtualKeyPosition.x, -virtualKeyPosition.y, strokeId);

                if (points.Count != 0)
                {
                    lastPoint = points[points.Count - 1];
                }

                if (lastPoint.X != virtualKeyPosition.x && lastPoint.Y != -virtualKeyPosition.y || points.Count == 0)
                {
                    points.Add(new Point(virtualKeyPosition.x, -virtualKeyPosition.y, strokeId));
                    currentStrokeIdCount++;
                }
            }
        }
    }


    public void RecognizeGesture()
    {
        if (currentStrokeIdCount == 1)
        {
            points.RemoveAt(points.Count - 1);
        }

        if (points.Count > 2)
        {
            Gesture candidate = new Gesture(points.ToArray());
            Result gestureResult = PointCloudRecognizer.Classify(candidate, trainingSet.ToArray());

            if (gestureResult.GestureClass == currentGestureName && gestureResult.Score > 0.85)
            {
                gestureIsMatch = true;
            }
            else
            {
                gestureIsMatch = false;
            }
        }
    }


    public List<Point> GetPoints()
    {
        return points;
    }


    public void ClearPoints()
    {
        strokeId = -1;
        points.Clear();
        gestureIsMatch = false;
    }

    public string GetPathToXML()
    {
        return pathToXML;
    }


    public void SaveCurrentData()
    {
        string currentTime = DateTime.Now.ToString();
        currentTime = currentTime.Replace("/", "_").Replace(" ", "_").Replace(":", "_");

        fileName = String.Format("{0}/{1}-{2}.xml", pathToXML, currentGestureName, currentTime);

        GestureIO.WriteGesture(points.ToArray(), currentGestureName, fileName);
        trainingSet.Add(new Gesture(points.ToArray(), currentGestureName));
    }


    public bool PointsIsEmpty()
    {
        return points.Count == 0;
    }


    public bool GetMatchState()
    {
        return gestureIsMatch;
    }


    public void SetMatchState(bool state)
    {
        gestureIsMatch = state;
    }


    public void SetDrawArea(Rect inputArea)
    {
        drawArea = inputArea;
    }


    public void SetCurrentGestureName(string name)
    {
        currentGestureName = name;
    }


    public void DrawInputGesture()
    {
        if (points != null)
        {
            if (points.Count > 2)
            {
                for (int i = 0; i < points.Count - 1; i++)
                {
                    Vector2 lineStart = new Vector2(points[i].X, Screen.height + points[i].Y);
                    Vector2 lineEnd = new Vector2(points[i + 1].X, Screen.height + points[i + 1].Y);

                    if (lineStart != lineEnd && points[i].StrokeID == points[i + 1].StrokeID)
                    {
                        DrawLine(lineStart, lineEnd, lineTexture, 4);
                    }
                }
            }
        }
    }


    void DrawLine(Vector2 lineStart, Vector2 lineEnd, Texture2D texture, int thickness)
    {
        Vector2 lineVector = lineEnd - lineStart;
        float angle = Mathf.Rad2Deg * Mathf.Atan(lineVector.y / lineVector.x);
        if (lineVector.x < 0)
        {
            angle += 180;
        }

        if (thickness < 1)
        {
            thickness = 1;
        }

        // The center of the line will always be at the center
        // regardless of the thickness.
        int thicknessOffset = (int)Mathf.Ceil(thickness / 2);

        Rect drawingRect = new Rect(lineStart.x,
                                    lineStart.y - thicknessOffset,
                                    Vector2.Distance(lineStart, lineEnd),
                                    (float)thickness);
        GUIUtility.RotateAroundPivot(angle, lineStart);
        GUI.BeginGroup(drawingRect);
        {
            int drawingRectWidth = Mathf.RoundToInt(drawingRect.width);
            int drawingRectHeight = Mathf.RoundToInt(drawingRect.height);

            for (int y = 0; y < drawingRectHeight; y += texture.height)
            {
                for (int x = 0; x < drawingRectWidth; x += texture.width)
                {
                    GUI.DrawTexture(new Rect(x, y, texture.width, texture.height), texture);
                }
            }
        }
        GUI.EndGroup();
        GUIUtility.RotateAroundPivot(-angle, lineStart);
    }
}
