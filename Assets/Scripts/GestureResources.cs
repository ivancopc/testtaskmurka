﻿using UnityEngine;
using System.Collections;

public class GestureResources : MonoBehaviour
{
    public Texture2D triangleUp, triangleDown, circle, square, hourglassUp, hourglassRight, fiveStar;

    string[] gesturesList = new string[] { "Triangle up", "Triangle down", "Circle", "Square", "Hourglass up", "Hourglass right", "Five star" };



    public string[] GetListGestures()
    {
        return gesturesList;
    }


    public Texture2D GetTexture(string name)
    {
        switch (name)
        {
            case "Triangle up": return triangleUp;
            case "Triangle down": return triangleDown;
            case "Circle": return circle;
            case "Square": return square;
            case "Hourglass up": return hourglassUp;
            case "Hourglass right": return hourglassRight;
            case "Five star": return fiveStar;
            default: return null;
        }
    }


    public string GetRandomGesture()
    {
        int numGesture = Random.Range(0, gesturesList.Length);

        return gesturesList[numGesture];
    }
}
