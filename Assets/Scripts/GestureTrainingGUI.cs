﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using PDollarGestureRecognizer;

public class GestureTrainingGUI : MonoBehaviour
{
    public GUISkin buttonsAreaSkin;
    public GUISkin informationAreaSkin;

    public float buttonsLeftAreaWidthPercent = 20.0f;
    public float buttonsRightAreaWidthPercent = 10.0f;
    public float informationAreaHeightPercent = 10.0f;

    public float leftButtonHeightPercent = 5.0f;
    public int padding = 5;

    public Texture2D exit, save, clear;
    public Texture2D match, notMatch;
    
    GestureResources resources;
    GestureManager gestureManager;

    float buttonsLeftAreaWidth, buttonsRightAreaWidth, informationAreaHeight;
    float leftButtonWidth, leftButtonHeight;
    float posXRightButtonsArea, posXRightButtons;
    float rightButtonsSize;
    float posYButtonExit, posYButtonClear, posYButtonSave;

    float informationAreaWidth, posXInformationArea, posYInformationArea;

    float gestureAreaWidth, gestureAreaHeight, gestureImageSize, posXGestureImage, posYGestureImage;

    float nameGestureHeight, pathToXMLHeihgt;

    string[] gesturesList = null;
    int numCurrentGesture = 0;

    

    
    public Rect GetDrawArea()
    {
        return new Rect(buttonsLeftAreaWidth, informationAreaHeight, gestureAreaWidth, gestureAreaHeight);
    }
    

    void Start()
    {
        resources = transform.GetComponent<GestureResources>();
        gestureManager = transform.GetComponent<GestureManager>();

        if (resources)
        {
            gesturesList = resources.GetListGestures();
        }

        if(gestureManager)
        {
            gestureManager.SetCurrentGestureName(gesturesList[numCurrentGesture]);
        }

        CalculationSizes();
    }
	

	void OnGUI()
    {
        CalculationSizes();
        DrawLeftButtonsArea();
        DrawRightButtonsArea();
        DrawInformationArea();
        DrawGestureImage();
        DrawInputGesture();
    }


    private void DrawInputGesture()
    {
        gestureManager.SetDrawArea(GetDrawArea());
        gestureManager.DrawInputGesture();
    }


    private void CalculationSizes()
    {
        buttonsLeftAreaWidth = GetWidthFromPercent(buttonsLeftAreaWidthPercent);
        buttonsRightAreaWidth = GetWidthFromPercent(buttonsRightAreaWidthPercent);
        informationAreaHeight = GetHeightFromPercent(informationAreaHeightPercent);

        leftButtonWidth = buttonsLeftAreaWidth - padding * 2;
        leftButtonHeight = GetHeightFromPercent(leftButtonHeightPercent);

        posXRightButtonsArea = Screen.width - buttonsRightAreaWidth;

        rightButtonsSize = buttonsRightAreaWidth - padding * 2;
        posXRightButtons = padding;
        posYButtonExit = padding;
        posYButtonSave = Screen.height - rightButtonsSize - padding;
        posYButtonClear = Screen.height - rightButtonsSize * 2 - padding * 2;

        informationAreaWidth = Screen.width - buttonsLeftAreaWidth - buttonsRightAreaWidth;
        posXInformationArea = buttonsLeftAreaWidth;
        posYInformationArea = Screen.height - informationAreaHeight;

        gestureAreaWidth = Screen.width - buttonsLeftAreaWidth - buttonsRightAreaWidth;
        gestureAreaHeight = Screen.height - informationAreaHeight;
        if(gestureAreaWidth < gestureAreaHeight)
        {
            gestureImageSize = gestureAreaWidth;
            posXGestureImage = 0;
            posYGestureImage = (gestureAreaHeight - gestureImageSize) / 2;
        }
        else
        {
            gestureImageSize = gestureAreaHeight;
            posXGestureImage = (gestureAreaWidth - gestureImageSize) / 2;
            posYGestureImage = 0;
        }

        nameGestureHeight = Mathf.Round(informationAreaHeight * 2 / 3);
        pathToXMLHeihgt = informationAreaHeight - nameGestureHeight;
    }


    private void DrawGestureImage()
    {
        GUI.skin = informationAreaSkin;
        GUI.BeginGroup(new Rect(posXInformationArea, 0, gestureAreaWidth, gestureAreaHeight));
        
        if(gesturesList != null)
        {
            if(gesturesList.Length > 0)
            {
                GUI.Label(new Rect(posXGestureImage, posYGestureImage, gestureImageSize, gestureImageSize), resources.GetTexture(gesturesList[numCurrentGesture]));
            }
        }
        
        GUI.EndGroup();
    }


    private void DrawInformationArea()
    {
        GUI.skin = informationAreaSkin;
        GUI.BeginGroup(new Rect(posXInformationArea, posYInformationArea, informationAreaWidth, informationAreaHeight));
        GUI.Box(new Rect(0, 0, informationAreaWidth, informationAreaHeight), "");

        if (gesturesList != null)
        {
            if (gesturesList.Length > 0)
            {
                informationAreaSkin.label.fontSize = 30;
                GUI.Label(new Rect(0, 0, informationAreaWidth, nameGestureHeight), gesturesList[numCurrentGesture]);
            }
        }

        informationAreaSkin.label.fontSize = 14;
        GUI.Label(new Rect(0, nameGestureHeight, informationAreaWidth, pathToXMLHeihgt), "Path to the XML files: " + gestureManager.GetPathToXML());

        if(gestureManager.GetMatchState())
        {
            GUI.Label(new Rect(0, 0, informationAreaHeight, informationAreaHeight), match);
        }
        else
        {
            GUI.Label(new Rect(0, 0, informationAreaHeight, informationAreaHeight), notMatch);
        }
        

        GUI.EndGroup();
    }


    private void DrawRightButtonsArea()
    {
        GUI.skin = buttonsAreaSkin;
        GUI.BeginGroup(new Rect(posXRightButtonsArea, 0, buttonsRightAreaWidth, Screen.height));
        GUI.Box(new Rect(0, 0, buttonsRightAreaWidth, Screen.height), "");

        if (GUI.Button(new Rect(posXRightButtons, posYButtonExit, rightButtonsSize, rightButtonsSize), exit))
        {
            SceneManager.LoadScene("MainMenu");
        }

        if(gestureManager.PointsIsEmpty())
        {
            GUI.enabled = false;
        }

        if (GUI.Button(new Rect(posXRightButtons, posYButtonSave, rightButtonsSize, rightButtonsSize), save))
        {
            Save();
        }

        GUI.enabled = true;

        if (GUI.Button(new Rect(posXRightButtons, posYButtonClear, rightButtonsSize, rightButtonsSize), clear))
        {
            Clear();
        }

        GUI.EndGroup();
    }


    private void Clear()
    {
        gestureManager.ClearPoints();
    }


    private void Save()
    {
        gestureManager.SaveCurrentData();
    }


    private void DrawLeftButtonsArea()
    {
        GUI.skin = buttonsAreaSkin;
        GUI.BeginGroup(new Rect(0, 0, buttonsLeftAreaWidth, Screen.height));
        GUI.Box(new Rect(0, 0, buttonsLeftAreaWidth, Screen.height), "");

        if (gesturesList != null && gesturesList.Length != 0)
        {
            float topPos = padding;

            for (int i = 0; i < gesturesList.Length; i++)
            {
                if (GUI.Button(new Rect(padding, topPos, leftButtonWidth, leftButtonHeight), gesturesList[i]))
                {
                    numCurrentGesture = i;
                    gestureManager.SetCurrentGestureName(gesturesList[numCurrentGesture]);
                    Clear();
                }

                topPos += leftButtonHeight + padding;
            }
        }

        GUI.EndGroup();
    }


    float GetWidthFromPercent(float percent)
    {
        return (Screen.width * percent) / 100.0f;
    }


    float GetHeightFromPercent(float percent)
    {
        return (Screen.height * percent) / 100.0f;
    }
}
