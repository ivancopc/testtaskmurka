﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverMenu : Menu
{
    GameManager gameManager;

    protected override void Start()
    {
        base.Start();

        gameManager = transform.GetComponent<GameManager>();
    }


    public void SetHeader(string inputName)
    {
        header = inputName;
    }


    protected override void SetButtons()
    {
        buttons = new string[] { "Restart Game", "Main Menu" };
    }

    protected override void HandleButton(string text)
    {
        switch (text)
        {
            case "Restart Game": RestartGame(); break;
            case "Main Menu": MainMenu(); break;
            default: break;
        }
    }

    private void RestartGame()
    {
        gameManager.StartGame();
    }


    private void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
