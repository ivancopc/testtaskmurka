﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : Menu
{
    protected override void SetButtons()
    {
        buttons = new string[] { "Game", "Gestures training", "Quit Game" };
    }

    protected override void HandleButton(string text)
    {
        switch (text)
        {
            case "Game": NewGame(); break;
            case "Gestures training": GestureTraining(); break;
            case "Quit Game": ExitGame(); break;
            default: break;
        }
    }

    private void NewGame()
    {
        SceneManager.LoadScene("Game");
    }


    private void GestureTraining()
    {
        SceneManager.LoadScene("GestureTraining");
    }
}