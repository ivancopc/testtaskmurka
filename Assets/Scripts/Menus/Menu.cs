﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    public GUISkin mySkin;
    public string header;
    public float buttonHeightPercent = 10.0f;
    public float buttonWidthPercent = 30.0f;
    public float headerHeightPercent = 15.0f;
    public float menuWidthPercent = 40.0f;
    public int padding = 5;

    protected string[] buttons;

    protected virtual void Start()
    {
        SetButtons();
    }

    protected virtual void OnGUI()
    {
        DrawMenu();
    }

    protected virtual void DrawMenu()
    {
        // Default implementation for a menu consisting of a vertical list of buttons
        GUI.skin = mySkin;
        float menuHeight = GetMenuHeight();
        float menuWidth = GetWidthFromPercent(menuWidthPercent);

        float groupLeft = (Screen.width - menuWidth) / 2;
        float groupTop = (Screen.height - menuHeight) / 2;
        float headerHeight = GetHeightFromPercent(headerHeightPercent);

        float buttonsHeight = GetHeightFromPercent(buttonHeightPercent);
        float buttonsWidth = GetWidthFromPercent(buttonWidthPercent);

        GUI.BeginGroup(new Rect(groupLeft, groupTop, menuWidth, menuHeight));

        //background box
        GUI.Box(new Rect(0, 0, menuWidth, menuHeight), "");
        //header image
        GUI.Label(new Rect(0, padding, menuWidth, headerHeight), header);

        //menu buttons
        if (buttons != null)
        {
            float leftPos = (menuWidth - buttonsWidth) / 2;
            float topPos = 2 * padding + headerHeight;
            for (int i = 0; i < buttons.Length; i++)
            {
                if (GUI.Button(new Rect(leftPos, topPos, buttonsWidth, buttonsHeight), buttons[i]))
                {
                    HandleButton(buttons[i]);
                }

                topPos += buttonsHeight + padding;
            }
        }

        GUI.EndGroup();
    }


    protected virtual void SetButtons()
    {
        //A child class needs to set this for buttons to appear
    }


    protected virtual void HandleButton(string text)
    {
        //A child class needs to set this to handle button clicks
    }


    protected virtual float GetMenuHeight()
    {
        float menuHeight = 0.0f;
        float buttonHeight = GetHeightFromPercent(buttonHeightPercent);
        float headerHeight = GetHeightFromPercent(headerHeightPercent);

        if (buttons != null)
        {
            menuHeight = buttons.Length * (buttonHeight + padding) + headerHeight + padding * 2;
        }

        return menuHeight;
    }


    protected virtual float GetWidthFromPercent(float percent)
    {
        return (Screen.width * percent) / 100.0f;
    }


    protected virtual float GetHeightFromPercent(float percent)
    {
        return (Screen.height * percent) / 100.0f;
    }


    protected void ExitGame()
    {
        Application.Quit();
    }
}