﻿using UnityEngine;
using System.Collections;

public class MouseFollow : MonoBehaviour
{
    public float distance = 10;

	
	void Update ()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 position = ray.GetPoint(distance);
        transform.position = position;
	}
}
