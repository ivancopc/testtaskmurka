﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartGameMenu : Menu
{
    GameManager gameManager;

    protected override void Start()
    {
        base.Start();

        gameManager = transform.GetComponent<GameManager>();
    }


    protected override void SetButtons()
    {
        buttons = new string[] { "Start Game", "Main Menu" };
    }

    protected override void HandleButton(string text)
    {
        switch (text)
        {
            case "Start Game": StartGame(); break;
            case "Main Menu": MainMenu(); break;
            default: break;
        }
    }

    private void StartGame()
    {
        gameManager.StartGame();
    }


    private void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
